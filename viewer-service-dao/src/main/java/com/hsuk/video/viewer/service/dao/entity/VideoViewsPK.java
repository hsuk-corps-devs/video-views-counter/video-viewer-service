package com.hsuk.video.viewer.service.dao.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Data
@Embeddable
public class VideoViewsPK implements Serializable {

    @Column(name = "video_id")
    private long videoId;

    @Column(name = "start_period")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startPeriod;

    @Column(name = "end_period")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endPeriod;
}
