package com.hsuk.video.viewer.service.dao.dto;

import com.hsuk.video.viewer.service.dao.entity.VideoInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@RedisHash("videoInfo")
public class VideoInfoDto extends ResponseData {

    @Id
    private long videoId;
    private String videoUrl;
    private String videoName;
    private String videoDescription;
    private transient Date uploadDate;
    private long uploaderId;

    public long getVideoId() {
        return videoId;
    }

    public void setVideoId(long videoId) {
        this.videoId = videoId;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public String getVideoDescription() {
        return videoDescription;
    }

    public void setVideoDescription(String videoDescription) {
        this.videoDescription = videoDescription;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public long getUploaderId() {
        return uploaderId;
    }

    public void setUploaderId(long uploaderId) {
        this.uploaderId = uploaderId;
    }

    public static VideoInfoDto convertFromEntity(VideoInfo info) {

        VideoInfoDto dto = new VideoInfoDto();
        BeanUtils.copyProperties(info, dto);

        return dto;
    }
}
