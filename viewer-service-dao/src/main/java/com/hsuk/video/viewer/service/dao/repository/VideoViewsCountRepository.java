package com.hsuk.video.viewer.service.dao.repository;

import com.hsuk.video.viewer.service.dao.entity.VideoViewsCount;
import com.hsuk.video.viewer.service.dao.entity.VideoViewsPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface VideoViewsCountRepository extends JpaRepository<VideoViewsCount, VideoViewsPK> {

    @Query("select SUM(v.viewsCount) from VideoViewsCount v where v.videoViewsPK.videoId=?1 AND v.videoViewsPK.startPeriod>=?2" +
            " AND v.videoViewsPK.endPeriod<=?3")
     Integer getTotalViewsCount(long videoId, Date startFrom, Date startTo);

}