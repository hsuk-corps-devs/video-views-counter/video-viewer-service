package com.hsuk.video.viewer.service.dao.service;

import com.hsuk.platform.utils.dynaconfig.dto.ApplicationDto;
import com.hsuk.platform.utils.dynaconfig.service.ApplicationService;
import com.hsuk.platform.utils.dynaconfig.util.DynamicPropUtil;
import com.hsuk.video.viewer.service.dao.dto.VideoInfoDto;
import com.hsuk.video.viewer.service.dao.dto.VideoViewsCountDto;
import com.hsuk.video.viewer.service.dao.entity.VideoInfo;
import com.hsuk.video.viewer.service.dao.redis.repository.RedisVideoRepository;
import com.hsuk.video.viewer.service.dao.repository.VideoInfoRepository;
import com.hsuk.video.viewer.service.dao.repository.VideoViewsCountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class VideoViewsDao {

    private VideoInfoRepository videoInfoRepository;

    private VideoViewsCountRepository videoViewsCountRepository;

    private RedisVideoRepository redisVideoRepository;

    private ApplicationService applicationService;
    private DynamicPropUtil dynamicPropUtil;

    @Autowired
    public VideoViewsDao(RedisVideoRepository redisVideoRepository, VideoInfoRepository videoInfoRepository,
                         VideoViewsCountRepository videoViewsCountRepository, DynamicPropUtil dynamicPropUtil, ApplicationService applicationService) {
        this.redisVideoRepository = redisVideoRepository;
        this.videoInfoRepository = videoInfoRepository;
        this.applicationService = applicationService;
        this.videoViewsCountRepository = videoViewsCountRepository;
        this.dynamicPropUtil = dynamicPropUtil;
    }

    @PostConstruct
    public void initApp() {
        ApplicationDto app = new ApplicationDto();
        app.setAppId(dynamicPropUtil.getInt("app.Id"));
        app.setAppDescription(dynamicPropUtil.getStr("app.description"));
        app.setAppName(dynamicPropUtil.getStr("app.name"));
        app.setCreatedAt(new Date());
        app.setModifiedAt(new Date());
        app.setCreatedBy(dynamicPropUtil.getStr("app.name"));

        applicationService.saveApplication(app);
    }

    public VideoViewsCountDto getViewsCount(long videoId, Date startPeriod, Date endPeriod) {
        VideoViewsCountDto videoViewsCountDto = new VideoViewsCountDto();
        videoViewsCountDto.setTimeStamp(new Date());
        Integer viewsCount = videoViewsCountRepository.getTotalViewsCount(videoId, startPeriod, endPeriod);

        if (viewsCount != null) {
            videoViewsCountDto.setCount(viewsCount);
        }

        videoViewsCountDto.setMessage("SUCCESS");

        return videoViewsCountDto;
    }

    public void saveVideoInfo(VideoInfoDto videoInfoDto) {
        VideoInfo videoInfo = new VideoInfo();
        BeanUtils.copyProperties(videoInfoDto, videoInfo);
        videoInfoRepository.save(videoInfo);
    }

    public void saveVideoInfos(List<VideoInfoDto> videoInfoDto) {
        List<VideoInfo> entities = videoInfoDto.stream().map(val -> {
                    VideoInfo videoInfo = new VideoInfo();
                    BeanUtils.copyProperties(val, videoInfo);
                    return videoInfo;
                }
        ).collect(Collectors.toList());

        videoInfoRepository.saveAll(entities);
    }

    public VideoInfoDto getVideoInfo(long videoId) {
        try {

            Optional<VideoInfoDto> dto = redisVideoRepository.findById(String.valueOf(videoId));

            VideoInfoDto videoInfoDto;

            if (!dto.isPresent()) {
                log.info("Could't find video of Id:{} on redis. Fetching from DB", videoId);

                Optional<VideoInfo> optional = videoInfoRepository.findById(videoId);

                videoInfoDto = optional.map(VideoInfoDto::convertFromEntity).orElse(null);

                if (videoInfoDto != null) {
                    log.info("Found video with Id:{} on DB, saving to Redis.", videoId);
                    redisVideoRepository.save(videoInfoDto);
                } else {
                    log.info("Invalid video Id:{} . Can't find video.", videoId);
                }

            } else {
                log.info("Found video of Id:{} on redis.", videoId);
                videoInfoDto = dto.get();
            }

            return videoInfoDto;
        } catch (Exception ex) {
            log.error("Couldn't get video info for Id:{}", videoId);
        }

        return null;
    }

}
