package com.hsuk.video.viewer.service.dao.dto;

import lombok.Data;

@Data
public class VideoPlayRequest {

    private String videoUrl;
    private String sessionId;
    private String userId;

}
