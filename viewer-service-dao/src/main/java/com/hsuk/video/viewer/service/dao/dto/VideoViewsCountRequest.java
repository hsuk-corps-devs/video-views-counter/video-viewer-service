package com.hsuk.video.viewer.service.dao.dto;

import lombok.Data;

import java.util.Date;

@Data
public class VideoViewsCountRequest {

    private String videoUrl;

    private Date startDateTime;

    private Date endDateTime;
}
