package com.hsuk.video.viewer.service.dao.config;

import com.hsuk.platform.utils.dynaconfig.util.DynamicPropUtil;
import com.hsuk.video.viewer.service.dao.config.convertors.BytesToTimeStampConvertor;
import com.hsuk.video.viewer.service.dao.config.convertors.TimeStampToBytesConverter;
import io.lettuce.core.ReadFrom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.convert.RedisCustomConversions;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.Arrays;

@Configuration
@ComponentScan("com.hsuk.video.views.dto")
@EnableRedisRepositories(basePackages = "com.hsuk.video.viewer.service.dao.redis.repository")
public class RedisConfig {

    private DynamicPropUtil dynamicPropUtil;

    @Autowired
    public RedisConfig(DynamicPropUtil dynamicPropUtil) {
        this.dynamicPropUtil = dynamicPropUtil;
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate() {
        RedisTemplate<String, String> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());
        template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());

        return template;
    }

    @Bean
    public RedisCustomConversions redisCustomConversions(BytesToTimeStampConvertor bytesToTimeStampConvertor,
                                                         TimeStampToBytesConverter timeStampToBytesConverter) {
        return new RedisCustomConversions(Arrays.asList(bytesToTimeStampConvertor, timeStampToBytesConverter));
    }

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {

        LettuceClientConfiguration clientConfig = LettuceClientConfiguration.builder()
                .readFrom(ReadFrom.REPLICA_PREFERRED)
                .build();

        String redisMasterHost = dynamicPropUtil.getStr("redis.master.host");
        String password = dynamicPropUtil.getStr("redis.server.password");
        int redisMasterPort = dynamicPropUtil.getInt("redis.master.port");


        RedisStandaloneConfiguration serverConfig = new RedisStandaloneConfiguration(redisMasterHost,redisMasterPort);
        serverConfig.setPassword(RedisPassword.of(password));
        return new LettuceConnectionFactory(serverConfig, clientConfig);
    }

}
