package com.hsuk.video.viewer.service.dao.config.convertors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class BytesToTimeStampConvertor implements Converter<Timestamp, byte[]> {

    @Override
    public byte[] convert(final Timestamp source) {
        return String.valueOf(source.getTime()).getBytes();
    }
}
