package com.hsuk.video.viewer.service.dao.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class VideoViewsCountDto extends ResponseData {

    private int count;
}