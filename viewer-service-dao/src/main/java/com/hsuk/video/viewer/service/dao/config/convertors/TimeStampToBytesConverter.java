package com.hsuk.video.viewer.service.dao.config.convertors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class TimeStampToBytesConverter implements Converter<byte[], Timestamp> {

    @Override
    public Timestamp convert(byte[] bytes) {
        return new Timestamp(Long.parseLong(new String(bytes)));
    }
}
