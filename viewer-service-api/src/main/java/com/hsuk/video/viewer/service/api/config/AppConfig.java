package com.hsuk.video.viewer.service.api.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Slf4j
@Configuration
@EnableCaching
@PropertySources(value = {
        @PropertySource(name = "video.view.counter.db.global", value = "classpath:/db.properties"),
        @PropertySource(name = "video.view.counter.application", value = "classpath:/application.properties"),
        @PropertySource(name = "video.view.counter.application.env", value = "classpath:/application-${app.environment}.properties"),
        @PropertySource(name = "video.view.counter.db.env", value = "classpath:/db-${app.environment}.properties"),
        @PropertySource(name = "video.view.counter.override", value = "${loader.override.filepath}", ignoreResourceNotFound = true)
}
)
@ComponentScan({"com.hsuk.video.viewer", "com.hsuk.platform.utils"})
public class AppConfig {


}
