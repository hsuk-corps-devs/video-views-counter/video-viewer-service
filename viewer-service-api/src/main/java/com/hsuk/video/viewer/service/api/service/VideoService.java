package com.hsuk.video.viewer.service.api.service;

import akka.NotUsed;
import akka.stream.javadsl.Flow;
import com.hsuk.video.viewer.service.api.service.kafka.VideoViewMessage;
import com.hsuk.video.viewer.service.api.validator.UrlValidator;
import com.hsuk.video.viewer.service.dao.service.VideoViewsDao;
import com.hsuk.video.viewer.service.dao.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Slf4j
@Service
public class VideoService {

    private UrlValidator urlValidator;
    private VideoViewsDao videoViewsDao;
    private KafkaService kafkaService;

    @Autowired
    public VideoService(UrlValidator urlValidator, VideoViewsDao videoViewsDao, KafkaService kafkaService) {
        this.urlValidator = urlValidator;
        this.videoViewsDao = videoViewsDao;
        this.kafkaService = kafkaService;
    }

    public Flow<VideoPlayRequest, VideoPlayResponse, NotUsed> playVideo() {

        return Flow.of(VideoPlayRequest.class)
                .via(processPlay())
                .mapAsync(1, f -> f);
    }

    public Flow<VideoViewsCountRequest, VideoViewsCountDto, NotUsed> getCount() {

        return Flow.of(VideoViewsCountRequest.class)
                .via(processViewsCount())
                .mapAsync(1, f -> f);
    }


    public Flow<String, VideoInfoDto, NotUsed> getInfo() {

        return Flow.of(String.class)
                .via(processGetInfo())
                .mapAsync(1, f -> f);
    }

    private Flow<VideoViewsCountRequest, CompletionStage<VideoViewsCountDto>, NotUsed> processViewsCount() {

        return Flow.<VideoViewsCountRequest>create().map(req -> {
            UrlValidator.ValidationResult result = urlValidator.validate(req.getVideoUrl());

            if (result.isValid()) {
                long vid = result.getVideoId();
                VideoViewsCountDto videoViewsCountDto = videoViewsDao.getViewsCount(vid, req.getStartDateTime(), req.getEndDateTime());

                if (videoViewsCountDto != null) {

                    return CompletableFuture.completedFuture(videoViewsCountDto);
                }

            }

            VideoViewsCountDto notAvailable = getVideoCountErrorMsg(req.getVideoUrl());

            return CompletableFuture.completedFuture(notAvailable);
        });
    }

    private Flow<VideoPlayRequest, CompletionStage<VideoPlayResponse>, NotUsed> processPlay() {

        return Flow.<VideoPlayRequest>create().map(req -> {
            UrlValidator.ValidationResult result = urlValidator.validate(req.getVideoUrl());

            if (result.isValid()) {
                long vid = result.getVideoId();
                VideoInfoDto info = videoViewsDao.getVideoInfo(vid);

                if (info != null) {
                    // create kafka message object
                    VideoViewMessage message = new VideoViewMessage();
                    message.setVideoId(vid);
                    message.setUserId(req.getUserId());
                    message.setSessionId(req.getSessionId());
                    message.setTimestamp(System.currentTimeMillis());
                    kafkaService.writeIntoPipeline(message);

                    VideoPlayResponse videoPlayResponse = new VideoPlayResponse();
                    videoPlayResponse.setVideoName(info.getVideoName());
                    videoPlayResponse.setMessage("Playing video..");
                    videoPlayResponse.setDescription(info.getVideoDescription());
                    videoPlayResponse.setTimeStamp(new Date());
                    videoPlayResponse.setVideoUrl(req.getVideoUrl());

                    return CompletableFuture.completedFuture(videoPlayResponse);
                }

            }

            VideoPlayResponse notAvailable = getVideoPlayErrorMsg(req.getVideoUrl());

            return CompletableFuture.completedFuture(notAvailable);
        });

    }

    private Flow<String, CompletionStage<VideoInfoDto>, NotUsed> processGetInfo() {

        return Flow.<String>create().map(req -> {

            UrlValidator.ValidationResult result = urlValidator.validate(req);

            if (result.isValid()) {

                VideoInfoDto info = videoViewsDao.getVideoInfo(result.getVideoId());

                if (info != null) {

                    return CompletableFuture.completedFuture(info);

                }

            }

            VideoInfoDto notAvailable = getVideoInfoErrorMsg(req);

            return CompletableFuture.completedFuture(notAvailable);
        });

    }

    private VideoPlayResponse getVideoPlayErrorMsg(String url) {

        VideoPlayResponse videoPlayResponse = new VideoPlayResponse();
        videoPlayResponse.setError("Couldn't find video with url: " + url);
        videoPlayResponse.setTimeStamp(new Date());
        videoPlayResponse.setVideoUrl(url);

        return videoPlayResponse;

    }

    private VideoViewsCountDto getVideoCountErrorMsg(String url) {

        VideoViewsCountDto videoPlayResponse = new VideoViewsCountDto();
        videoPlayResponse.setError("Couldn't find video with url: " + url);
        videoPlayResponse.setTimeStamp(new Date());
        videoPlayResponse.setCount(0);

        return videoPlayResponse;

    }

    private VideoInfoDto getVideoInfoErrorMsg(String url) {
        VideoInfoDto videoInfoDto = new VideoInfoDto();
        videoInfoDto.setError("Couldn't find video with url: " + url);
        videoInfoDto.setTimeStamp(new Date());
        videoInfoDto.setVideoUrl(url);

        return videoInfoDto;
    }
}
