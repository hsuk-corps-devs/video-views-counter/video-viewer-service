package com.hsuk.video.viewer.service.api;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class VideoViewerAppMain {

    public static void main(String[] args) {
        new SpringApplicationBuilder(VideoViewerAppMain.class)
                .run(args);

    }
}
