package com.hsuk.video.viewer.service.api.route;

import akka.http.javadsl.server.Route;

public interface RouteConfig {

    Route createRoutes();
}
