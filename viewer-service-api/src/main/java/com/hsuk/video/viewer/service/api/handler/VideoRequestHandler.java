package com.hsuk.video.viewer.service.api.handler;

import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpHeader;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.ExceptionHandler;
import akka.http.javadsl.server.Route;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.hsuk.video.viewer.service.api.akka.AkkaConfig;
import com.hsuk.video.viewer.service.api.service.VideoService;
import com.hsuk.video.viewer.service.dao.dto.VideoInfoDto;
import com.hsuk.video.viewer.service.dao.dto.VideoPlayRequest;
import com.hsuk.video.viewer.service.dao.dto.VideoPlayResponse;
import com.hsuk.video.viewer.service.dao.dto.VideoViewsCountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;

@Component
public class VideoRequestHandler extends AllDirectives {

    private final LoggingAdapter log;
    private final HttpHeader applicationJson = HttpHeader.parse("Content-Type", "application/json");
    private final VideoService videoService;
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");

    @Autowired
    public VideoRequestHandler(AkkaConfig akkaConfig, VideoService videoService) {
        log = Logging.getLogger(akkaConfig.getSystem(), this);
        this.videoService = videoService;
    }

    public Route doPlayVideo(String videoUrl, String sessionId, String userId) {

        VideoPlayRequest request = new VideoPlayRequest();
        request.setSessionId(sessionId);
        request.setUserId(userId);
        request.setVideoUrl(videoUrl);

        final ExceptionHandler genericExceptionHandler = ExceptionHandler
                .newBuilder()
                .match(Exception.class,
                        x -> complete(Collections.singletonList(applicationJson),
                                getErrorPlayResponse(request.getVideoUrl(), x),
                                Jackson.marshaller()))
                .build();

        return extractMaterializer(mat -> onSuccess(() -> Source.single(request).via(videoService.playVideo()).
                        map(response -> response).
                        runWith(Sink.head(), mat),
                p -> handleExceptions(genericExceptionHandler,
                        () -> complete(StatusCodes.OK, p, Jackson.marshaller()))));
    }

    public Route getViewsCount(String videoUrl, String fromDateTime, String toDateTime) {
        VideoViewsCountRequest request = new VideoViewsCountRequest();
        try {
            request.setVideoUrl(videoUrl);
            request.setStartDateTime(DATE_FORMAT.parse(fromDateTime));
            request.setEndDateTime((DATE_FORMAT).parse(toDateTime));

        } catch (Exception ex) {
            log.error("Error building request: {}", ex.getMessage());
        }

        final ExceptionHandler genericExceptionHandler = ExceptionHandler
                .newBuilder()
                .match(Exception.class,
                        x -> complete(Collections.singletonList(applicationJson),
                                getErrorPlayResponse(request.getVideoUrl(), x),
                                Jackson.marshaller()))
                .build();

        return extractMaterializer(mat -> onSuccess(() -> Source.single(request).via(videoService.getCount()).
                        map(response -> response).
                        runWith(Sink.head(), mat),
                p -> handleExceptions(genericExceptionHandler,
                        () -> complete(StatusCodes.OK, p, Jackson.marshaller()))));
    }

    public Route doGetVideoInfo(String videoUrl) {

        final ExceptionHandler genericExceptionHandler = ExceptionHandler
                .newBuilder()
                .match(Exception.class,
                        x -> complete(Collections.singletonList(applicationJson),
                                getErrorInfoResponse(videoUrl, x),
                                Jackson.marshaller()))
                .build();

        return extractMaterializer(mat -> onSuccess(() -> Source.single(videoUrl).via(videoService.getInfo()).
                        map(response -> response).
                        runWith(Sink.head(), mat),
                p -> handleExceptions(genericExceptionHandler,
                        () -> complete(StatusCodes.OK, p, Jackson.marshaller()))));
    }

    private VideoPlayResponse getErrorPlayResponse(String videoUrl, Exception ex) {
        String errorMsg = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();
        log.info("Error playing video:{}, error:{}", videoUrl, errorMsg);

        VideoPlayResponse response = new VideoPlayResponse();
        response.setVideoUrl(videoUrl);
        response.setTimeStamp(new Date());
        response.setError("Can't play video: " + videoUrl + ", error:{} " + errorMsg);

        return response;
    }

    private VideoInfoDto getErrorInfoResponse(String videoUrl, Exception ex) {
        String errorMsg = ex.getCause() != null ? ex.getCause().getMessage() : ex.getMessage();

        log.info("Error Getting video info:{}, error:{}", videoUrl, errorMsg);

        VideoInfoDto response = new VideoInfoDto();
        response.setVideoUrl(videoUrl);
        response.setTimeStamp(new Date());
        response.setError("Can't get info about video: " + videoUrl + ", error:{} " + errorMsg);

        return response;
    }
}
