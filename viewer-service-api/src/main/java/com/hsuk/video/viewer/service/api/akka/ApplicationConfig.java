package com.hsuk.video.viewer.service.api.akka;


import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import com.hsuk.platform.utils.dynaconfig.util.DynamicPropUtil;
import com.hsuk.video.viewer.service.api.route.RouteConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.CompletionStage;

/**
 * Created by Sumit Shrestha on 1/10/2019.
 */
@Slf4j
@Component
public class ApplicationConfig extends AllDirectives {

    private final RouteConfig searchRoutesConfig;
    private final RouteConfig healthRoutesConfig;
    private final AkkaConfig akkaConfig;
    private final DynamicPropUtil dynamicPropUtil;
    private static final String LOCALHOST = "0.0.0.0";

    @Autowired
    public ApplicationConfig(@Qualifier("apiRouteConfig") RouteConfig searchRoutesConfig, @Qualifier("healthRouteConfig") RouteConfig healthRoutesConfig,
                             AkkaConfig akkaConfig, DynamicPropUtil dynamicPropUtil) {
        this.searchRoutesConfig = searchRoutesConfig;
        this.healthRoutesConfig = healthRoutesConfig;
        this.akkaConfig = akkaConfig;
        this.dynamicPropUtil = dynamicPropUtil;
    }

    public void initApp() {
        log.info("Initializing application..");

        final ActorSystem system = akkaConfig.getSystem();
        final ActorMaterializer materializer = akkaConfig.getMaterializer();

        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlowMain = this.concat(this.createSearchRouteConfig()
                /* , new SwaggerService().createRoute() */)
                .flow(system, materializer);

        final Flow<HttpRequest, HttpResponse, NotUsed> routeFlowHealth = this.concat(this.createHealthRouteConfig())
                .flow(system, materializer);

        log.info("main: Routes created");

        final Http http = Http.get(system);

        log.info("main: Done setting default server http context");

        int serverPort = dynamicPropUtil.getInt("server.port", 8090);
        int serverManagementPort = dynamicPropUtil.getInt("server.management.port", 9090);

        CompletionStage<ServerBinding> binding = http.bindAndHandle(routeFlowMain, ConnectHttp.toHost(LOCALHOST, serverPort), materializer);
        log.info("main: Server online at https://{}:{}", LOCALHOST, serverPort);

        log.info("main: Server health check at https://{}:{}", LOCALHOST, serverManagementPort);
        http.bindAndHandle(routeFlowHealth, ConnectHttp.toHost(LOCALHOST, serverManagementPort), materializer);

        akkaConfig.printConfig();

        registerOnDestroy(binding);
    }

    private Route createSearchRouteConfig() {

        return searchRoutesConfig.createRoutes();
    }

    private Route createHealthRouteConfig() {

        return healthRoutesConfig.createRoutes();
    }

    private void registerOnDestroy(CompletionStage<ServerBinding> binding) {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("onDestroy: Starting terminating ActorSystem");
            try {
                binding.thenCompose(ServerBinding::unbind)
                        .thenAccept(unbound -> akkaConfig.getSystem().terminate());
                log.info("onDestroy: Unbound http binding and Terminated ActorSystem");
            } catch (Exception ex) {
                log.info("onDestroy: Terminating ActorSystem exception " + ex);
            }
        }));
    }

    @PostConstruct
    public void onApplicationEvent() {
        initApp();
    }
}