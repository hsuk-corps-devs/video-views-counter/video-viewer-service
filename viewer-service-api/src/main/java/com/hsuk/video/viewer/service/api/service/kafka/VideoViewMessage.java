package com.hsuk.video.viewer.service.api.service.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * POJO class to containing data to be sent to kafka
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VideoViewMessage {
    private long videoId;
    private String sessionId;
    private long timestamp;
    private String userId;
}
