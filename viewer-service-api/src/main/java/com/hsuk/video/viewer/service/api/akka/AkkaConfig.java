package com.hsuk.video.viewer.service.api.akka;

import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AkkaConfig {
    private static final Logger logger = LoggerFactory.getLogger(AkkaConfig.class);
    private final ActorSystem system;
    private final ActorMaterializer materializer;

    AkkaConfig() {
        Config regularAkkaConfig = ConfigFactory.load();

        this.system = ActorSystem.create("gdsSearchHttpServer", regularAkkaConfig);
        this.materializer = ActorMaterializer.create(this.system);
    }

    public ActorSystem getSystem() {
        return this.system;
    }

    public ActorMaterializer getMaterializer() {
        return this.materializer;
    }

    public String getConfigStr(String key) {
        return this.system.settings().config().getString(key);
    }

    public int getConfigInt(String key) {
        return this.system.settings().config().getInt(key);
    }

    public void printConfig() {
        Map<String, String> env = System.getenv();

        for (Map.Entry<String, String> entry : env.entrySet()) {
            logger.info("printConfig:{}, env vars: {}", entry.getKey(), entry.getValue());
        }
    }
}
